--------------------------------------------------------------------------------
Switcher languages module
--------------------------------------------------------------------------------
This module allows users with administer languages permissions to alter the
display of the language switcher block links. They can configure various
display settings on the language switcher configuration page.

Getting started
---------------
Go to the language switcher configuration page:
/admin/structure/block/manage/locale/language/configure.

In the "Languages" fieldset:
- Check "Disable active language" to hide the active language link.
- Check "Display language code" to replace the language titles by their
  language codes.
- Select the languages you want to display.

You can combine any of these settings.

--------------------------------------------------------------------------------
Developer information
--------------------------------------------------------------------------------
The locale module calls the theme hook suggestion links__locale_block in its
hook_block_view. As theme_links__locale_block() can not be called in a module
and to avoid multiple calls to hook_preprocess_links(), this module declares
links__locale_block as a theme function.

Language codes
--------------
If you choose to display the language codes instead of the language names, the
language codes are displayed with the first letter uppercase. You can use
CSS text-transform to display all letters lowercase or uppercase.